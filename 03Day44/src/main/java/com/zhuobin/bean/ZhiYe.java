package com.zhuobin.bean;

import java.io.Serializable;

public class ZhiYe implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 8767676546L;

	private Integer zyId;

    private String zyName;

    public Integer getZyId() {
        return zyId;
    }

    public void setZyId(Integer zyId) {
        this.zyId = zyId;
    }

    public String getZyName() {
        return zyName;
    }

    public void setZyName(String zyName) {
        this.zyName = zyName == null ? null : zyName.trim();
    }
}