package com.zhuobin.bean;

import java.io.Serializable;

public class Employee implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 45454545L;

	private Integer eId;

    private String eName;

    private Integer eAge;

    private String eSex;

    private ZhiYe zhiye;

    public Integer geteId() {
        return eId;
    }

    public void seteId(Integer eId) {
        this.eId = eId;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName == null ? null : eName.trim();
    }

    public Integer geteAge() {
        return eAge;
    }

    public void seteAge(Integer eAge) {
        this.eAge = eAge;
    }

    public String geteSex() {
        return eSex;
    }

    public void seteSex(String eSex) {
        this.eSex = eSex == null ? null : eSex.trim();
    }

	public ZhiYe getZhiye() {
		return zhiye;
	}

	public void setZhiye(ZhiYe zhiye) {
		this.zhiye = zhiye;
	}

   
}