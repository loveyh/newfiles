package com.zhuobin.dao;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.zhuobin.bean.ZhiYe;
@MapperScan
public interface ZhiYeMapper {
    int deleteByPrimaryKey(Integer zyId);

    int insert(ZhiYe record);

    int insertSelective(ZhiYe record);

    ZhiYe selectByPrimaryKey(Integer zyId);
    List<ZhiYe> findAll();
    int updateByPrimaryKeySelective(ZhiYe record);

    int updateByPrimaryKey(ZhiYe record);
}