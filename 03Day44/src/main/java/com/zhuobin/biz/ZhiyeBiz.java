package com.zhuobin.biz;
import java.util.List;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import com.zhuobin.bean.ZhiYe;
import com.zhuobin.dao.ZhiYeMapper;
@Service
public class ZhiyeBiz {
	@Resource
	private ZhiYeMapper zyDao;
	public List<ZhiYe> findAll(){
		return zyDao.findAll();
	}
	public ZhiYe findById(int zy_id){
		return zyDao.selectByPrimaryKey(zy_id);
	}
}