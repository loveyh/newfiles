package com.zhuobin.biz;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.zhuobin.bean.Employee;
import com.zhuobin.dao.EmployeeMapper;
@Service
public class EmployeeBiz {
	@Resource
	private EmployeeMapper empDao;
	public List<Employee> findAll(){
		return empDao.findAll();
	}
	public Employee findById(int e_id){
		return empDao.selectByPrimaryKey(e_id);
	}
	public void save(Employee employee){
		empDao.insert(employee);
	}
	public void update(Employee employee){
		empDao.updateByPrimaryKey(employee);
	}
	public void delete(Employee employee){
		empDao.deleteByPrimaryKey(employee.geteId());
	}
}
