<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<base href="<%= request.getContextPath()%>/">
</head>
<body>
<f:form action="employee" method="GET">
	<input type="submit" value="添加"/>
</f:form>
<table width="800px" align="center" border="1">
	<tr>
		<th>员工编号</th>
		<th>员工姓名</th>
		<th>员工年龄</th>
		<th>员工性别</th>
		<th>职业id</th> 
		<th>职业名称</th>
		<th>操作</th>
	</tr>
	<c:forEach var="emp" items="${list }">
		<tr>
			<td>${emp.eId }</td>
			<td>${emp.eName }</td>
			<td>${emp.eAge }</td>
			<td>${emp.eSex }</td>
			<td>${emp.zhiye.zyId}</td>
			<td>${emp.zhiye.zyName}</td>
			<td>
			<a href="employees/${emp.eId}">修改</a>   
			<f:form action="employees/${emp.eId}" method="DELETE">
				<input type="submit" value="删除"/>
			</f:form>
			</td>	
		</tr>
	</c:forEach>
</table>
</body>
</html>